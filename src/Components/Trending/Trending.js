import React, { useState, useEffect } from 'react';
import './Trending.css';
// import ProgramImg from '../../Images/Program_b.svg';
// import Government_p from '../../Images/Government_p.svg'
import ReguTax_img from "../../Images/regutax_img.svg"
import axios from "axios"
import { message, Modal, } from 'antd';
import Skeleton from 'react-loading-skeleton';
import Interweave from 'interweave';
import ReactPlayer from 'react-player';
import screenfull from 'screenfull'
import { findDOMNode } from 'react-dom'
import { ReactComponent as PlayBtn } from "../../Images/playbtn.svg"
import { ReactComponent as FullScreenBtn } from "../../Images/fullscreen.svg"
import { ReactComponent as PauseBtn } from "../../Images/pause.svg"
import jwt from "jsonwebtoken";
import 'antd/dist/antd.css';




function Trending(props) {
    const [articles, setarticles] = useState("")
    const [title, settitle] = useState("")
    const [body, setbody] = useState("")
    const [photo1, setphoto] = useState("")
    const [loader, setloader] = useState(false)
    const [videoname, setvideoname] = useState("")
    const [thetitle, setthetitle] = useState('none')
    const [dispimg, setdispimg] = useState([])
    const [seeking, setseeking] = useState(false)
    const [playing, setplaying] = useState(true)
    const [theplayer, setplayer] = useState('')
    // const [mainbtn, setmainbtn] = useState("none")
    const [played, setplayed] = useState(0)
    // const [seeker, setseeker] = useState("none")
    // const [videoid, setvideoid] = useState("")
    const [videourl, setvideourl] = useState("")
    const [thumbnail, setthumbnail] = useState("")
    // const [playerbtn, setplayerbtn] = useState("none")
    // const [fullscrbtn, setfullscrbtn] = useState("none")
    // const [videolink, setvideolink] = useState("")
    // const [theimages, settheimages] = useState([])
    const [visible, setvisible] = useState(false)
    const [theviddisp, setviddisp] = useState("none")
    const [thetext, settext] = useState("none")
    const[ImageVisible, SetImageVisible] = useState(false)
    const [imagebtndisp, setimagebtndisp] = useState("none")
    // const [videoname, setvideoname] = useState("")
    const ABC = <div className="prg-def-cors">Coming Soon</div>
    useEffect(() => {
        // 5ef1e932bb42614561a01d89
        axios.get('https://counsel.apimachine.com/api/getlawarticlesbyfilter/ReguTax/5ef1e932bb42614561a01d89')
            // 5f0446f92269a1726b3cc12a
            .then(response => {
                if (response.data.success) {
                    // setTimeout(getarticles, 100);
                    setarticles(response.data.data)

                } else {
                    message.error(response.data.message)
                }
            }).catch(err => console.log(err))
    }, [])
    console.log("cover-photo" + JSON.stringify(articles))
    const handlePause = () => {
        console.log('onPause')
        setplaying(false)
    }
    const handlePlay = () => {
        console.log('onPlay')
        setplaying(true)
    }
    const handlePlayPause = () => {
        setplaying(!playing)
    }
    const ref = player => {
        setplayer(player)
    }
    const handleClickFullscreen = () => {
        screenfull.request(findDOMNode(theplayer))
    }
    const handleDuration = (duration) => {
        console.log('onDuration', duration)
        // setduration(duration)
    }
    const handleSeekChange = (e) => {
        setplayed(parseFloat(e.target.value))
    }

    const handleSeekMouseDown = e => {
        setseeking(true)
    }
    const handleSeekMouseUp = e => {
        theplayer.seekTo(parseFloat(e.target.value))
    }
    const handleProgress = state => {
        console.log('onProgress', state)
        setplayed(state.played)
    }
    if (articles.length === 0) {
        return (
            <div className="full-prg">
                <div className="prg-crs">
                    {ABC}{ABC}{ABC}{ABC}{ABC}
                </div>
                <div className="trnd-cont">
                    <div className="row">
                        <div className="col-8">
                            <Skeleton height={50} />
                            <br /><br />
                            <Skeleton count={5} />
                        </div>
                        <div className="pr-0 col-4">
                            <div className="bt-comb">
                                <div className="btns-t">
                                    <span className="mr-3 theclr theapply">APPLY WITH</span><img src={ReguTax_img} alt="no_image" className="regutaximg" />
                                </div>
                                <div>
                                    <div className="bt-grs theclr">InstaLegal Quote</div>
                                    <div className="btns-t-not theclr">
                                        Sell You Inactive Business <br />Instantly
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    const ShowModal = async (e) => {
        setvisible(true)
        const hide = message.loading("Loading Video", 0)
        console.log("thetitle1" + e.target.name)
        articles.forEach(async (element) => {
            if (element.Title === e.target.name) {
                console.log("videoid1 " + element.VideoLink)
                setthumbnail(element.VideoThumbnail)
                if (element.VideoName || element.VideoName === "" || element.VideoName === null || element.VideoName === undefined) {
                    settext("block")
                }



                const secret = '9xr7w8KCuY2Wmhe89bR';


                const token = jwt.sign({}, secret, {
                    algorithm: "HS512",
                    expiresIn: 240,
                    issuer: "gxervench215",
                });
                let stream_link = await axios.post(
                    `https://vod-backend.globalxchange.io/get_dev_upload_video_stream_link?token=${token}`,
                    {
                        "video_id": element.VideoLink
                    },
                    {
                        headers: {
                            "Access-Control-Allow-Origin": "*",
                        },
                    }
                );

                console.log(JSON.stringify(stream_link) + " videourl1");
                // console.log(videoid + " videourl1")
                // console.log(videolink + " videourl")
                if (stream_link.data === "Video not found") {
                    settext("block")
                    setTimeout(hide, 100);
                    setviddisp("none")
                } else {
                    setvideourl(stream_link.data)
                    setTimeout(hide, 100);
                    settext("none")
                    setviddisp("block")
                }
            }
        })

    }
    const ShowImageModal = (e)=>{
        SetImageVisible(true)
    }
    const handleOk = (e)=>{
        SetImageVisible(false)
    }
    const handleCancel = (e)=>{
        SetImageVisible(false)
    }
    const handleOk1 = (e) => {
        setvisible(false)
    }
    const handleCancel1 = (e) => {
        setvisible(false)
    }
    const theimage = async (e) => {
        setloader(true)
        setTimeout(
            () => setloader(false),
            2000
        );
        console.log((photo1) + " photoos")
        articles.forEach(async (element) => {
            if (element.CoverPhoto === e.target.src) {
                console.log(element.Body + ' Body--')
                if (element.Images !== null) {
                    setdispimg(element.Images)
                }
                if(element.Images === null || element.Images.length === 0 ){
                    setimagebtndisp("none")
                }else{
                    setimagebtndisp("block")
                }
                if (!element.VideoName || element.VideoName === "" || element.VideoName === null || element.VideoName === undefined) {

                    setthetitle("none")
                } else {
                    setvideoname(element.VideoName)
                    setthetitle('block')
                }
                // if(element.VideoName === "" || element.VideoName === null || element.VideoName === undefined){
                //     setthetitle("none")
                // }else{
                //     setthetitle("block")
                // }
                setbody(element.Body)
                // setvideoname(element.VideoName)
                settitle(element.Title)
                setthumbnail(element.VideoThumbnail)


            }
        });
    }

    if (title === "") {
        settitle(articles[0].Title)
        if (articles[0].VideoName === "" || articles[0].VideoName === null || articles[0].VideoName === undefined) {
            setthetitle('none')
            console.log("test123 " + articles[0].VideoName)
        } else {
            setthetitle('block')
            setvideoname(articles[0].VideoName)
            console.log("test123 " + articles[0].VideoName)
        }
        if(articles[0].Images === null || articles[0].Images.length === 0 ){
            setimagebtndisp("none")
        }else{
            setimagebtndisp("block")
        }
        setdispimg(articles[0].Images)
        setvideoname(articles[0].VideoName)
    }
    if (body === "") {
        setbody(articles[0].Body)
    }
    let sample
    if (articles.length === 0) {
        sample = ["1", "2", "3", "4", "5"]
    } else if (articles.length === 1) {
        sample = ["1", "2", "3", "4"]
    } else if (articles.length === 2) {
        sample = ["1", "2", "3"]
    } else if (articles.length === 3) {
        sample = ["1", "2"]
    } else if (articles.length === 4) {
        sample = ["1"]
    } else {
        sample = []
    }
    if (loader === true) {
        return (
            <div className="full-prg">
                <div className="prg-crs">
                    {articles.map(photo => {
                        return <div style={{ marginLeft: "30px" }}>
                            <input className="thedisp" onClick={theimage} type="image" src={photo.CoverPhoto} className="theimgs" alt="no_image" />
                        </div>
                    })}
                    {sample.map(element => {
                        return ABC
                    })}
                </div>
                <div className="trnd-cont">
                    <div className="row">
                        <div className="col-8">
                            <Skeleton height={50} />
                            <br /><br />
                            <Skeleton count={5} />
                        </div>
                        <div className="pr-0 col-4">
                            <div className="bt-comb">
                                <div className="btns-t">
                                    <span className="mr-3 theclr theapply">APPLY WITH</span><img src={ReguTax_img} alt="no_image" className="regutaximg" />
                                </div>
                                <div>
                                    <div className="bt-grs theclr">InstaLegal Quote</div>
                                    <div className="btns-t-not theclr">
                                        Sell You Inactive Business <br />Instantly
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div className="full-prg">
                <div className="prg-crs">
                    {articles.map(photo => {
                        return <div style={{ marginLeft: "30px" }}>
                            <input className="thedisp" onClick={theimage} type="image" src={photo.CoverPhoto} className="theimgs" alt="no_image" />
                        </div>
                    })}
                    {sample.map(element => {
                        return ABC
                    })}
                    {/* {ABC}{ABC}{ABC}{ABC}{ABC}{ABC} */}
                </div>
                <div className="trnd-cont">
                    <div className="row">
                        <div className="col-8">
                            <h2 className="theclr">{title}</h2>
                            <p style={{ textAlign: "justify" }}><Interweave content={body} /></p>
                            <br />
                            <div style={{ display: "flex" }}>
                                <input type="button" className="thepropbtn" onClick={ShowImageModal} style={{ textAlign: "center", display:imagebtndisp }} value="Preview Images" />
                                <Modal title= "Images" visible={ImageVisible} onOk={handleOk} onCancel={handleCancel}>
                                    {dispimg.map(element => {
                                        return <>
                                            <img src={element.image} type="image" className="thedisp themodalimg" alt="no_image" />
                                   &nbsp;&nbsp;&nbsp;
                                   </>
                                    })}
                                </Modal>
                            </div>
                            <br />
                            <h3 style={{ display: thetitle }}>Checkout the video for detailed Information:</h3>
                            <br />
                            {/* <h3 className="theclr" >{videoname}</h3><br /> */}
                            <input type="button" className="thepropbtn" onClick={ShowModal} style={{ textAlign: "center", display: thetitle }} value="Preview Video" name={title} />
                            <Modal title={videoname} visible={visible} onOk={handleOk1} onCancel={handleCancel1}>
                                <p style={{ display: thetext }}>No Video Available</p>

                                <ReactPlayer
                                    style={{ display: theviddisp }}
                                    ref={ref}
                                    url={videourl}
                                    light={thumbnail}
                                    controls={false}
                                    className="theplayer-sty"
                                    width="100%"
                                    height="310px"
                                    onPause={handlePause}
                                    playing={playing}
                                    onPlay={handlePlay}
                                    pip={false}
                                    onSeek={e => console.log('onSeek', e)}
                                    onProgress={handleProgress}
                                    onDuration={handleDuration}
                                />
                                <div >
                                    <p onClick={handlePlayPause} style={{ display: theviddisp }} className="theplaypausebtn-sty">{playing ? <PauseBtn /> : <PlayBtn />}</p>
                                    <p onClick={handleClickFullscreen} style={{ display: theviddisp }} className="thefullscrn-btn "><FullScreenBtn /></p>
                                </div>
                                <input
                                    type='range' min={0} max={0.999999} step='any'
                                    className="theplayerseek1"
                                    value={played}
                                    style={{ display: theviddisp }}
                                    onMouseDown={handleSeekMouseDown}
                                    onChange={handleSeekChange}
                                    onMouseUp={handleSeekMouseUp}
                                />
                            </Modal>


                        </div>

                        <div className="pr-0 col-4">
                            <div className="bt-comb">
                                <div className="btns-t">
                                    <span className="mr-3 theclr theapply">APPLY WITH</span><img src={ReguTax_img} alt="no_image" className="regutaximg" />
                                </div>
                                <div>
                                    <div className="bt-grs theclr">InstaLegal Quote</div>
                                    <div className="btns-t-not theclr">
                                        Sell You Inactive Business <br />Instantly
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Trending;
