import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import './NavbarTop.css';
import Government_p from '../../Images/Government_p.svg'
import ReguTax_img from "../../Images/regutax_img.svg"
import MenuIconA from '../../Images/menu-icon-inbox.svg';
import CounselLogo from "../../Images/counsellogo.png"
// import MyselfIcon from '../../Images/mselfIcon.svg';
// import CompanyIcon from '../../Images/CompanyIcon.png';
// import NonProfitIcon from '../../Images/NonProfitIcon.svg';
// import JobIcon from '../../Images/JobIcon.svg';
import { ProductConsumer } from '../../Context_Api/Context';

const MenuDivs = <div className="d-flex">
    <div className="mn-left-hlf">
        <div className="d-flex align-items-center">
            <img src={MenuIconA} width="18px" alt="no_img" />
            <p className="menu-sm-heads">Inbox</p>
        </div>
        <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
    </div>
    <div className="mn-right-hlf">
        <div className="d-flex align-items-center">
            <img src={MenuIconA} width="18px" alt="no_img" />
            <p className="menu-sm-heads">Inbox</p>
        </div>
        <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
    </div>
</div>

export default class NavbarTop extends Component {
    render() {
        return (
            <ProductConsumer>
                {((value) => {
                    const { TopmenuFullOpen, menuVersionName } = value;
                    console.log('menuVersionName',menuVersionName)
                    return (
                        <>
                            <Navbar collapseOnSelect expand="lg" className="navstyle">
                                {/* <div className="container"> */}
                                <div className="w-100" style={{ minHeight: "inherit" }}>
                                    {/* <div className="tp-sm-nav">
                                        <div className="container-fluid">
                                            <div className="w-100 p-2">
                                                <Nav>
                                                    <div className="ml-auto d-flex align-items-center" style={{ cursor: "pointer" }} onClick={TopmenuFullOpen}>
                                                        {menuVersionName==='For Businesses'?
                                                            <img src={MyselfIcon} width="20px" className="mr-3" alt="no_img" />
                                                        :null} 
                                                        {menuVersionName==='For Individuals'?
                                                            <img src={CompanyIcon} width="20px" className="mr-3" alt="no_img" />
                                                        :null}
                                                        {menuVersionName==='For NGO’s'?
                                                            <img src={JobIcon} width="20px" className="mr-3" alt="no_img" />
                                                        :null}
                                                        {menuVersionName==='I’m A Non-Profit'?
                                                            <img src={NonProfitIcon} width="20px" className="mr-3" alt="no_img" />
                                                        :null} 
                                                        <span style={{fontWeight:"700", color:"#000"}}>{menuVersionName}</span> <i className="fas fa-angle-down ml-3" style={{color:"#000"}}/>
                                                    </div>
                                                </Nav>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className="container-fluid padd-cont-cust">
                                        <div className="nav-d-menu">
                                            <Navbar.Brand href="/" className="brand-desk"><img src={ReguTax_img} alt="no_img" /></Navbar.Brand>
                                            <div className="tog-n-cols">
                                            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                                            <Navbar.Collapse id="responsive-navbar-nav" style = {{marginTop:"12px"}}>
                                                <Nav>
                                                    {/* <NavDropdown title="Services" className="Pord-menu-one">
                                                        <div className="row p-0 m-0">
                                                            <div className="col-md-8 p-0 m-0">
                                                                <div className="p-4 h-100">
                                                                    <div className="mb-4">
                                                                        <small style={{ color: "#9aa8bd" }}>PRODUCTS</small>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <div className="mn-left-hlf">
                                                                            <div className="d-flex align-items-center">
                                                                                <img src={MenuIconA} width="18px" alt="no_img" />
                                                                                <p className="menu-sm-heads">Inbox</p>
                                                                            </div>
                                                                            <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
                                                                        </div>
                                                                        <div className="mn-right-hlf">
                                                                            <div className="d-flex align-items-center">
                                                                                <img src={MenuIconA} width="18px" alt="no_img" />
                                                                                <p className="menu-sm-heads">Inbox</p>
                                                                            </div>
                                                                            <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
                                                                        </div>
                                                                    </div>
                                                                    {MenuDivs}{MenuDivs}{MenuDivs}
                                                                </div>
                                                            </div>
                                                            <div className="col-md-4 p-0 m-0">
                                                                <div className="p-4 h-100" style={{ backgroundColor: "#f1f3f8" }}>
                                                                    <div className="mb-4">
                                                                        <small style={{ color: "#9aa8bd" }}>CHANNELS</small>
                                                                    </div>
                                                                    <div className="in-rt-mn-word">WhatsApp</div>
                                                                    <div className="in-rt-mn-word">SMS</div>
                                                                    <div className="in-rt-mn-word">Voice</div>
                                                                    <div className="in-rt-mn-word">Email</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </NavDropdown>
                                                    <NavDropdown title="Solutions" className="Sol-menu-two">
                                                        <div className="row p-0 m-0">
                                                            <div className="col-md-8 p-0 m-0">
                                                                <div className="p-4 h-100">
                                                                    <div className="mb-4">
                                                                        <small style={{ color: "#9aa8bd" }}>PRODUCTS</small>
                                                                    </div>
                                                                    <div className="mn-left-hlf">
                                                                        <div className="d-flex align-items-center">
                                                                            <img src={MenuIconA} width="18px" alt="no_img" />
                                                                            <p className="menu-sm-heads">Global 2FA</p>
                                                                        </div>
                                                                        <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
                                                                    </div>
                                                                    <div className="mn-right-hlf">
                                                                        <div className="d-flex align-items-center">
                                                                            <img src={MenuIconA} width="18px" alt="no_img" />
                                                                            <p className="menu-sm-heads">Inbox for Good</p>
                                                                        </div>
                                                                        <p className="menu-sm-paras">Free, omnichannel customer support and engagement</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-4 p-0 m-0">
                                                                <div className="p-4 h-100" style={{ backgroundColor: "#f1f3f8", minWidth: "200px" }}>
                                                                    <div className="mb-4">
                                                                        <small style={{ color: "#9aa8bd" }}>CHANNELS</small>
                                                                    </div>
                                                                    <div className="in-rt-mn-word">Ecommerce</div>
                                                                    <div className="in-rt-mn-word">On Demand</div>
                                                                    <div className="in-rt-mn-word">Logistics</div>
                                                                    <div className="in-rt-mn-word">Healthcare</div>
                                                                    <div className="in-rt-mn-word">Financial Services</div>
                                                                    <div className="in-rt-mn-word">Insurance</div>
                                                                    <div className="in-rt-mn-word">Sales & Marketing</div>
                                                                    <div className="in-rt-mn-word">Financial</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </NavDropdown> */}
                                                    <Nav.Link className = "navclr newhover">Legislation<span class="pic arrow-down"></span></Nav.Link>
                                                    <Nav.Link className = "navclr newhover">Lawyers<span class="pic arrow-down"></span></Nav.Link>
                                                    <Nav.Link className = "navclr newhover">Taxes<span class="pic arrow-down"></span></Nav.Link>
                                                    {/* <div className="tp-sm-nav"> */}
                                                        <div className="container-fluid newhover">
                                                            <div className= "thebox py-1 px-4" style={{border: "0.5px solid #E5E5E5"}}>
                                                                <Nav>
                                                                    <div className="ml-auto d-flex align-items-center navclr" style={{ cursor: "pointer" }} >
                                                                        {/* {menuVersionName==='For Businesses'?
                                                                            <img src={MyselfIcon} width="20px" className="mr-3" alt="no_img" />
                                                                        :null} 
                                                                        {menuVersionName==='For Individuals'?
                                                                            <img src={CompanyIcon} width="20px" className="mr-3" alt="no_img" />
                                                                        :null}
                                                                        {menuVersionName==='I’m A Non-Profit'?
                                                                            <img src={NonProfitIcon} width="20px" className="mr-3" alt="no_img" />
                                                                        :null}  */}
                                                                        <span className = "app-store"><img src = {CounselLogo} alt = "logo"/>&nbsp;&nbsp;App Store</span>
                                                                    </div>
                                                                </Nav>
                                                            </div>
                                                        </div>
                                                    {/* </div> */}
                                                </Nav>
                                            </Navbar.Collapse>
                                            </div>
                                            <Navbar.Brand href="/" className="brand-desk-mob"><img src={ReguTax_img} alt="no_img" style = {{marginTop:"-6px"}}/></Navbar.Brand>
                                        </div>
                                    </div>
                                    {/* <hr style = {{marginTop:"0px"}}/> */}
                                </div>
                                {/* </div> */}
                            </Navbar>
                        </>
                    )
                })}
            </ProductConsumer>
        )
    }
}
