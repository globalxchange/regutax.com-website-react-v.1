import React, { Component } from 'react';

const ProductContext = React.createContext();
const Ppara = <p>Originally launched on April 9, 2020, CEBA is intended to support businesses by providing financing for their expenses that cannot be avoided or deferred as they take steps to safely navigate a period of shutdown, thereby helping to position businesses for successful relaunch when the economy reopens.<br/><br/>
This $55 billion program provides interest-free loans of up to $40,000 to small businesses and not-for-profits.<br/><br/>
Repaying the balance of the loan on or before December 31, 2022 will result in loan forgiveness of 25 percent (up to $10,000).<br/><br/>
As of October 26, 2020, eligibility for CEBA has expanded by removing the previous March 1, 2020, condition for having an active business chequing/operating account. With this removal, eligible businesses can now apply after opening a business chequing/operating account with their primary financial institution. For more information about this new criteria or other criteria established since the launch of CEBA we recommend you visit the FAQs.
</p>

class ProductProvider extends Component {
  state = {
    menuFullDrop: false,
    menuVersionName: 'For Businesses',
    homeHeadName: 'India’s Largest Legal Database',
    homeParaName: 'For Business Owners',
    allPrograms: [
      {id : '1', prgHead: 'Canada Emergency Business Account', prgPara: Ppara}
    ]
  }
  
TopmenuFullOpen = () => {
  this.setState({
    menuFullDrop: true 
  });
}
TopmenuFullClose = () => {
  this.setState({
    menuFullDrop: false
  });
}
forMyselfVersion = () => {
  this.setState({
    menuVersionName: 'For Businesses', menuFullDrop: false, homeHeadName: 'India’s Largest Legal Database', homeParaName: 'For Business Owners'
});}
forMyCompanyVersion = () => {
  this.setState({
    menuVersionName: 'For Individuals', menuFullDrop: false, homeHeadName: 'India’s Largest Legal Database', homeParaName: 'For Individuals'
});}
// forMyJobVersion = () => {
//   this.setState({
//     menuVersionName: 'For NGO’s', menuFullDrop: false, homeHeadName: 'Get Paid To Talk', homeParaName: 'Join OnHold As An Agent For Your Favourite Brands'
// });}
forMyNonprofitVersion = () => {
  this.setState({
    menuVersionName: 'For NGO’s', menuFullDrop: false, homeHeadName: 'India’s Largest Legal Database', homeParaName: 'For NGOs'
});}


render() { 
    return (
      <ProductContext.Provider value={{
        ...this.state,

        TopmenuFullOpen: this.TopmenuFullOpen,
        TopmenuFullClose: this.TopmenuFullClose,
        forMyselfVersion: this.forMyselfVersion,
        forMyCompanyVersion: this.forMyCompanyVersion,
        // forMyJobVersion: this.forMyJobVersion,
        forMyNonprofitVersion: this.forMyNonprofitVersion,

      }}>
        {this.props.children}
      </ProductContext.Provider>
    )

  }
}

const ProductConsumer = ProductContext.Consumer;

export { ProductProvider, ProductConsumer };